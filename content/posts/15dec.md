---
title: "15 December:: Manekshaw's Order, Requests of Niazi"
date: 2022-09-17T00:43:37+06:00
draft: false
---

Indian Army Chief General Sam Manekshaw ordered Pakistani Forces to surrender for the last time. Before that General A A K Niazi requested a armistice through US Consul General in Dhaka Herbert D Spivak to the US embassy in Delhi to the Indian government. Major General Rao Forman Ali signed as a witness in Niazi's message. US Embassy handed the message over to General Sam Manekshaw, who ordered to halt the airstrikes in Dhaka from 5pm to 9am in the 16th december.

Manekshaw asked Niazi in reply, to surrender rather than organizing armistice, through US embassy. He also said that the land-forces and Mukti Bahini would continue to advance, while only the airstrike would observe armistice. And he also added that if Niazi surrenders unconditionally within 9am the next day, he would be given full opportunity of the Geneva convention.

#### Joutho Bahini on suburban Dhaka:

Mukti Bahini and Mitra Bahini (Indian Allied Forces) fired at different targets of Pak Forces within 2 miles of Dhaka. Pak Army retaliated the whole day. Crossing Meghna river in Daudkandi, the Joutho Bahini encircled the whole city.
