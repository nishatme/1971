---
title: "16 December 1971:: Bangladesh emerges, winning the Liberation War"
date: 2022-09-16T23:34:25+06:00
draft: false
---

Through nine long months of armed warfare, on this day, Bangalee nation got it's much anticipated victory in the Liberation War of 1971. Through this victory, the people of East Bengal gave birth to Bangladesh, a new state in the world map. A tearful and joyous moment was curated by the "Joy Bangla" chant by thousands in Dhaka. Defeated Pakistan Army's Chief of Eastern Wing Lieutenant General Amir Abdullah Khan Niazi formally surrendered arms and troops to Lietenant General Jagjit Singh Arora, Chief of Joutho Bahini (United Force)---made up of Mukti Bahini and Indian Forces---by signing the Instrument of Surrender at Race Course, Dhaka. A.K. Khandker, the Joint Commander of Mukti Bahini, represented Mukti Bahini at the ceremony. S-Force Commander Leutenant Colonel K.M. Shafiullah, Sector 2 Commander Major A.T.M. Hayder and Kader Siddiqui, among others, were also present on the Bangladeshi side at the ceremony.

![image_1](/images/16dec.jpeg)

Before that, from 5 in the morning, Pakistani Forces were observing truce, due to order from General Niazi. By 8 in the morning, a military zeep drove Major General Gandharv Singh Nagra, the divisional chief of the Indian Forces, to Mirpur Bridge. Adorned with white flag, two military officers from the Joutho Bahini drove towards Pakistan Army's Eastern Wing headquarters carrying message from General Nagra to General Niazi. The message read, "Dear Abdullah, I am at Mirpur Bridge. Send your representative."

Even before that, General Niazi requested the Defense Attaché at the American Consul office in Dhaka to inform India of his truce and armstice. Indian government were informed of that through the US Embassy in Delhi. By 11 am, the two representatives from Joutho Bahini returned from Niazi's headquarter. As a representative from General Niazi, Major General Jamshed brought formal Instrument of Surrender to General Nagra. General Nagra went to General Niazi with him at 12 in the noon. Then began talks and deliberation on the conditions of surrender. At around 1 pm, Major J F R Jacob---the Chief of Staff of the Eastern Command of Indian Army---reached Dhaka. By then, Joutho Bahini started driving deep in Dhaka, crossing the Mirpur bridge.

Thousands of people---defying the carfew---came out in the streets of Dhaka in excitement, upon hearing the news of the surrender in foreign radio channels. Dhanmondi and Puran Dhaka saw cheerful procession as early as 11 am. As time passed, the streets were filled in by more and more valiant Mukti Bahini fighters.

At 1 pm, the talks on writing the Instrument of Surrender (IoS) were started in Pakistan Army Headquarters. On one side were Niazi, Rao Forman Ali and Jamshed; on the other side were Jacob and Nagra. Niazi agreed to sign the IoS. It was then decided that, General Gagjit Singh Arora---the Joutho Bahini Chief---would sign the document in lieu of the victors.

In the evening, that historic IoS was signed at the Racecource Field (now, Suhrawardy Field), right where Bangabandhu declared, *"Ebarer Songram Amader Muktir Songram, Ebarer Songram Sadhinotar Songram"* (lit. this time the struggle is for our liberation, and for our independence) in the Historic 7th March Speech.

General Niazi reached Racecourse at 3:45pm. Soldiers, both winning and defeating, presented Guard of Honour to him for the last time.

The clock was exactly at 4 o'clock. Niazi and Arora sat on a chair placed in the middle of Racecourse. Niazi attempted to sign the IoS, but soon found out that he didn't have pen with him. With the pen of an Indian soldier, General Niazi signed the IoS and accepted this one of a kind (till then) defeat of a regular army against common masses. The clock saw 4:01pm.

After signing the IoS, General Niazi stood up and immediately put off his Badge of Rank from his shoulder. Then he brought out his revolver and peeled every bullet out of it. He then surrendered these badge and bullets and revolver to General Arora.

One news report of Associated Press (AP) sent from Dhaka that day said that Niazi was full of tears when he surrendered at Racecourse.

Prime Minister of Bangladesh government, Tajuddin Ahmed declared in Mujibnagar that the capital would be transferred to Dhaka the next week.

### Dhaka, the capital of the newly independent country

Indian Standard Time (IST) 5:00pm: the lower house (Loksabha) of the Indian parliament was absolutely full of it's capacity. The Prime Minister of the country, Indira Gandhi entered the house and said, "I want to declare something. I think this congregation is eager to hear this for some time now. The West Pakistani forces have surrenderred unconditionally. Dhaka is now an independent capital of an independent country!"

Indira Gandhi congratulated the people of Bangladesh and praised the brave Mukti Bahini.
